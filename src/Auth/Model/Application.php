<?php
/**
 * Copyright 2016 Andrew O'Rourke
 */

namespace Auth\Model;

/**
 * @package Auth\Model
 */
class Application extends Base
{
    public static $_table = 'applications';
}