# Alliance Authentication Portal

This is a work in progress central authentication portal for alliances within EVE: Online. It allows for a single login for alliance members across, all their characters, to multiple alliance services.

## Main Features

* None yet completed

## Roadmap

* Central Authentication for Pilots
* Role Management
* Permissions Management
* Automated Role Allocation
* OAuth 2 API
* OpenID Connect
* Automatic Federation
* Two-factor Authentication

## Requirements

* PHP 5.6+
* MySQL Database

## Installation

* Something Something Composer

## Other

### Author

* [Andrew O'Rourke (AndrewMontagne)](https://montagne.uk)

### Licence

The source code of this project is licenced under the [BSD 3-Clause Licence](https://opensource.org/licenses/BSD-3-Clause).